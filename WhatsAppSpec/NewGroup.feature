Feature: New Group
  As a WhatsApp user
  I want to create groups
  So that I can talk with 2 (Two) or more friends simultaneously

  Scenario: User creates a New Group
    Given I am on the main Whatsapp screen
     When I click on the Menu button
      And I select "New Group" option
      And I choose the following contacts:
        | name      |
        | Amanda    |
        | Samanta   |
        | Frederico |
      And I click on the Next button
      And I fill up the group data:
        | name               | image       |
        | Concrete Solutions | cs-logo.jpg |
      And I click on the Confirm button
     Then The Group is Created

  Scenario: User sees error message when attempting to create an empty group
    Given I am on the main Whatsapp screen
     When I click on the Menu button
      And I select "New Group" option
      And I click on the Next button
     Then I should see an error message with text "You must select at least one contact"

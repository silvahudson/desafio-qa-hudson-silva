Feature: Send Location
  As a WhatsApp user
  I want to share my location
  So my contacts know where I am

  Scenario: User shares current location with a contact
    Given GPS is active
      And I am in the Contact's chat window
     When I click on the Attach button
      And I select the Location option
      And I choose my location by clicking in one of the listed options
     Then My location is sent to the contact

  Scenario: User sees alert message when sharing location with GPS disabled
    Given GPS is not active
      And I am in the Contact's chat window
     When I click on the Attach button
      And I select the option Location
     Then Alert is displayed with the message "Show location settings?"

  Scenario: User sends location manually without using list of places
    Given GPS is active
      And I am in the Contact's chat window
     When I click on the Attach button
      And I select the option Location
      And I click on the Increase Screen button
      And I browse the map to find my location
      And I click on the Send Location button
     Then My location is sent to the contact

class Checkout

  attr :total

  def initialize
    @total = 0
    @cont = Hash.new(0)
  end

  def scan(item)
    @total += PRICE[item]
    @cont[item] += 1
    case
    when item == "A"
      if @cont[item]%3 == 0
        @total -= 20
      end
    when item == "B"
      if @cont[item]%2 == 0
        @total -= 15
      end
    end
  end

  private
    PRICE = { "" => 0, "A" => 50, "B" => 30, "C" => 20, "D" => 15 }
end

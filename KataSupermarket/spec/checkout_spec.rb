require_relative "../lib/checkout.rb"

describe "Concrete Solutions: Desafio" do

  def price(goods)
    checkout = Checkout.new
    goods.split(//).each { |item| checkout.scan(item) }
    checkout.total
  end

  describe "Checkout" do

    context "Empty cart" do
      it "Returns Zero" do
        expect(price("")).to be(0)
      end
    end

    context "With 1 (one) item" do
      it "A retuns 50" do
        expect(price("A")).to be(50)
      end

      it "B retuns 30" do
        expect(price("B")).to be(30)
      end

      it "C retuns 20" do
        expect(price("C")).to be(20)
      end

      it "D retuns 15" do
        expect(price("D")).to be(15)
      end
    end

    context "Multiple items WITHOUT Special Prices cases" do
      it "AB cart returns 80" do
        expect(price("AB")).to be(80)
      end

      it "CDBA cart returns 115" do
        expect(price("CDBA")).to be(115)
      end

      it "AA cart returns 100" do
        expect(price("AA")).to be(100)
      end
    end

    context "Multiple items WITH Special Prices cases" do
      it "AAA cart returns 130" do
        expect(price("AAA")).to be(130)
      end

      it "AAAA cart returns 180" do
        expect(price("AAAA")).to be(180)
      end

      it "AAAAA cart returns 230" do
        expect(price("AAAAA")).to be(230)
      end

      it "AAAAAA cart returns 260" do
        expect(price("AAAAAA")).to be(260)
      end

      it "AAAB cart returns 160" do
        expect(price("AAAB")).to be(160)
      end

      it "AAABB cart returns 175" do
        expect(price("AAABB")).to be(175)
      end

      it "ABBBB cart returns 140" do
        expect(price("ABBBB")).to be(140)
      end

      it "AAABBD cart returns 190" do
        expect(price("AAABBD")).to be(190)
      end

      it "DABABA cart returns 190" do
        expect(price("AAABBD")).to be(190)
      end
    end

    context "Incremental scanning" do
      it "ABAAB individual scanning returns:
          0, 50, 80, 130, 160 and 175" do
        co = Checkout.new
        co.scan("");  expect(co.total).to be(0)
        co.scan("A"); expect(co.total).to be(50)
        co.scan("B"); expect(co.total).to be(80)
        co.scan("A"); expect(co.total).to be(130)
        co.scan("A"); expect(co.total).to be(160)
        co.scan("B"); expect(co.total).to be(175)
      end

      it "ABAABAAA individual scanning returns:
          0, 50, 80, 130, 160, 175, 225, 275 and 305" do
        co = Checkout.new
        co.scan("");  expect(co.total).to be(0)
        co.scan("A"); expect(co.total).to be(50)
        co.scan("B"); expect(co.total).to be(80)
        co.scan("A"); expect(co.total).to be(130)
        co.scan("A"); expect(co.total).to be(160)
        co.scan("B"); expect(co.total).to be(175)
        co.scan("A"); expect(co.total).to be(225)
        co.scan("A"); expect(co.total).to be(275)
        co.scan("A"); expect(co.total).to be(305)
      end

      it "ABAABBB individual scanning returns:
          0, 50, 80, 130, 160, 175, 205 and 220" do
        co = Checkout.new
        co.scan("");  expect(co.total).to be(0)
        co.scan("A"); expect(co.total).to be(50)
        co.scan("B"); expect(co.total).to be(80)
        co.scan("A"); expect(co.total).to be(130)
        co.scan("A"); expect(co.total).to be(160)
        co.scan("B"); expect(co.total).to be(175)
        co.scan("B"); expect(co.total).to be(205)
        co.scan("B"); expect(co.total).to be(220)
      end
    end
  end
end
